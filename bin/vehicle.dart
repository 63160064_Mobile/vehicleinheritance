void main(List<String> arguments) {
  Car car = Car("Mustang");
  print(car.getModelName());
  car.currentEngineStatus();
  car.driveForward();
  car.engineStart();
  car.currentEngineStatus();
  car.engineStart();
  car.drives();
  car.driveForward();
  car.driveBackward();
  car.engineStop();
  car.currentEngineStatus();
  car.engineStop();
}

abstract class Vehicle {
  late String modelName;
  late String engineStatus;

  void engineStart() {
    print("Vehicle Engine Started.");
  }

  void engineStop() {
    print("Vehicle Engine Stopped.");
  }

  void currentEngineStatus();
}

abstract class CarFunction {
  void driveForward();
  void driveBackward();
  void blowHorn();
}

class Car extends Vehicle with Wheels implements CarFunction {
  Car(String modelName) {
    this.modelName = modelName;
    engineStatus = "Off";
  }

  String getModelName() {
    return "Car model name is $modelName.";
  }

  @override
  void engineStart() {
    if (engineStatus == "Off") {
      engineStatus = "On";
      print("Car : $modelName Engine Started.");
    } else {
      print("Car : $modelName Engine is already started.");
    }
  }

  @override
  void engineStop() {
    if (engineStatus == "On") {
      engineStatus = "Off";
      print("Car : $modelName Engine Stopped.");
    } else {
      print("Car : $modelName is already stopped.");
    }
  }

  @override
  void blowHorn() {
    print("Car : $modelName Blow Horn.");
  }

  @override
  void driveBackward() {
    if (engineStatus == "On") {
      print("Car : $modelName Drives Backward.");
    } else {
      print("Car : $modelName can't drive backward because the engine is off.");
    }
  }

  @override
  void driveForward() {
    if (engineStatus == "On") {
      print("Car : $modelName Drives Forward.");
    } else {
      print("Car : $modelName can't drive forward because the engine is off.");
    }
  }

  @override
  void currentEngineStatus() {
    if (engineStatus == "On") {
      print("Car : $modelName Engine is On.");
    } else {
      print("Car : $modelName Engine is Off.");
    }
  }

  @override
  void drives() {
    if (engineStatus == "On") {
      print("Car : $modelName is running with $wheels wheels.");
    } else {
      print("Car : $modelName can't drive because the engine is off.");
    }
  }
}

mixin Wheels {
  int wheels = 4;

  void drives() {
    print("Car is running with $wheels wheels.");
  }
}
